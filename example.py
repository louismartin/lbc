from lbc.scrapping import get_new_ads
from lbc.gpu import get_new_gpu_ads
from lbc.utils import trigger_alert

# Get GPUs and send alerts ("complex" logic inside method)
get_new_gpu_ads()

# Get Google Home ads
query = 'google home'
df, new_ids = get_new_ads(query, csv_name="google_home.csv")
if len(new_ids) > 0:
    alert_func = lambda row: row['price'] <= 30
    trigger_alert(df.loc[new_ids], alert_func, alert_name='Google Home')

[To prevent losing SSH connection after connecting to VPN on VPS](https://serverfault.com/questions/649792/prevent-ssh-connection-lost-after-logging-into-vpn-on-server-machine#comment1088800_747828)

Get public IP address of your local machine `$LOCAL_ADDR` and the gateway address of the VPS `$GATEWAY_ADDR`.

Prevent routing the traffic to your local machine via the VPN with `route add -host $LOCAL_ADDR gw $GATEWAY_ADDR`

[Download config files from openvpn](https://guide.tigervpn.com/en/articles/639219-linux-openvpn-setup)
Start openvpn with `cd config && sudo openvpn --config "AT - Vienna @tigervpn.com.ovpn"` with credentials provided in TigerVPN dashboard.

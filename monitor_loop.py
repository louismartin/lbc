import random
import time

from lbc.gpu import get_new_gpu_ads
from lbc.utils import print


# Get GPUs and send alerts ("complex" logic inside method)
while True:
    print('Getting new gpu ads')
    get_new_gpu_ads()
    time.sleep(1800 + random.randint(0, 120))

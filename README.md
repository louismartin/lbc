## Heroku setup
Set AWS credentials in heroku with
```bash
heroku config:set AWS_ACCESS_KEY_ID=...
heroku config:set AWS_SECRET_ACCESS_KEY=...
```

Deploy to heroku with
```bash
heroku ps:scale worker=1
```

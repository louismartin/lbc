import time
import re
from urllib.parse import urlencode
from urllib.error import HTTPError, URLError

from bs4 import BeautifulSoup
import pandas as pd
import requests

from lbc.utils import REPO_DIR
from lbc.openvpn import openvpn


def url2soup(url):
    """Fetch a webpage a return a BeautifulSoup soup object from its HTML"""
    try:
        headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"}
        with openvpn(REPO_DIR / 'config/AT - Vienna @tigervpn.com.ovpn', REPO_DIR / 'config/auth.txt'):
            response = requests.get(url, headers=headers)
            response.raise_for_status()
        soup = BeautifulSoup(response.text, "html.parser")
    except (HTTPError, URLError, ConnectionResetError) as e:
        print("Error fetching {} : {}".format(url.lower(), e))
        raise e
    return soup


def parse_date(date_str):
    hour, minute = date_str.split(", ")[-1].split(":")
    hour = int(hour)
    minute = int(minute)

    if "Aujourd'hui" in date_str:
        day = int(time.strftime("%d"))
        month = int(time.strftime("%m"))
    elif "Hier" in date_str:
        day = int(time.strftime("%d")) - 1
        month = int(time.strftime("%m"))
    else:
        day, month = date_str.split(", ")[0].split(" ")
        day = int(day)
        month = ["jan", "fév", "mar", "avr", "mai", "juin",
                 "juil", "août", "sep", "oct", "nov", "déc"].index(month) + 1

    year = int(time.strftime("%Y"))
    date = time.mktime((year, month, day, hour, minute, 0, 0, 0, 0))
    return date


def parse_ad(soup):
    soup = soup.a

    # href starts with "//wwww.leboncoin.fr/...", only keep the last part
    url = soup["href"].replace("//www.leboncoin.fr/", "")
    ad_id = int(re.match(r"/.+/(\d+)\.htm/", url).groups()[0])
    soup = soup.section

    title = soup.find("span", {"itemprop": "name"}).text
    # Remove \n \t and leading/trailing spaces
    title = re.sub(r"[\n\t]", "", title).strip()

    location = soup.find("p", {"itemprop": "availableAtOrFrom"}).text
    # Remove \n \t and leading/trailing spaces
    location = re.sub(r"[\n\t]", "", location).strip()
    # Replace any number of whitespaces with one.
    location = re.sub(r"( +)", " ", location)
    price = soup.find("span", {"itemprop": "priceCurrency"}).text
    price = int(re.findall("[0-9]+", price)[0])
    posted_date = soup.find("p", {"itemprop": "availabilityStarts"}).text
    posted_date = re.sub(r"[\n\t]", "", posted_date).strip()
    posted_date = parse_date(posted_date)
    ad = {"url": url,
          "title": title,
          "location": location,
          "price": price,
          "posted_date": posted_date}
    return ad_id, ad


def get_new_ads(query):
    params = {
            "text": query,
            "location": "Paris__48.85790400439862_2.358842071208555_10000_20000",  # Paris+20km
            "search_in": "subject",
    }
    url = f"https://www.leboncoin.fr/recherche/?{urlencode(params)}"
    request_time = time.time()
    soup = url2soup(url)
    ads = soup.find_all("li", {"itemtype": "http://schema.org/Offer"})
    assert len(ads) > 0
    df = pd.DataFrame()
    for ad in ads:
        try:
            ad_id, ad_dict = parse_ad(ad)
            for key, value in ad_dict.items():
                df.loc[ad_id, key] = value
        except AttributeError as e:
            print(e)

    assert len(df) > 0
    df["last_update"] = request_time
    return df.sort_values("posted_date", ascending=False)


def update_with_new_ads(df, df_new_ads):
    # Update with new ads
    for index, row in df_new_ads.iterrows():
        for key, value in row.items():
            df.loc[index, key] = value
    oldest_posted_date = df_new_ads.loc[df_new_ads.index, "posted_date"].min()
    [request_time] = df_new_ads["last_update"].unique()
    # Find ads that were removed since last refresh
    # (ads that should appear on first page only)
    if "removed_date" not in df.columns:
        df["removed_date"] = None
    df.loc[
        (
            (df["posted_date"] >= oldest_posted_date) &
            (df["last_update"] < request_time) &
            (df["removed_date"].isnull())
        ),
        "removed_date"
    ] = request_time
    return df.sort_values("posted_date", ascending=False)

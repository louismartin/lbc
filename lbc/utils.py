from functools import lru_cache
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from pathlib import Path
import logging

import boto3
import pandas as pd


REPO_DIR = Path(__file__).resolve().parent.parent
DATA_DIR = REPO_DIR / "data"
DATA_DIR.mkdir(exist_ok=True)
# Replace print function with logging
logging.basicConfig(level=logging.INFO)
print = logging.info


def send_email(subject, body, to_addr="louisrtm@gmail.com"):
    from_addr = "lmrt.bot@gmail.com"

    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Subject'] = subject

    body = body
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(from_addr, "botpassword")
    text = msg.as_string()
    server.sendmail(from_addr, to_addr, text)
    server.quit()


def format_ad(row):
    text = ""
    text += "{}\n".format(row["title"])
    text += "{0:.0f} €\n".format(row["price"])
    text += "https://www.leboncoin.fr/{}\n".format(row["url"].strip('/'))
    return text


def trigger_alert(df, alert_func, alert_name, format_ad_method=format_ad):
    '''
    Sends an email if alert_func triggers an alert for any of df's rows.
    Args:
        df: pandas dataframe with ads to check for alerts.
        alert_func: function that takes a dataframe row as input and returns
            a boolean that is true when the row should trigger an alert.
    '''
    df_alert = df[df.apply(alert_func, axis=1)]
    if df_alert.shape[0] == 0:
        return
    subject = '{} alert'.format(alert_name)
    body = '\n'.join([format_ad_method(row) for _, row in df_alert.iterrows()])
    print(f'Sending email alert:\n{subject}\n{body}')
    try:
        send_email(subject=subject, body=body)
    except Exception as e:
        print(e)



@lru_cache()
def get_s3_client():
    return boto3.client(
            's3',
            aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
            aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY'),
    )


def is_s3_available():
    return 'AWS_ACCESS_KEY_ID' in os.environ and 'AWS_SECRET_ACCESS_KEY' in os.environ


def upload_to_s3(local_path, s3_path):
    print('Uploading file to S3')
    get_s3_client().upload_file(str(local_path), 'lrtm.bot', s3_path)


def download_from_s3(s3_path, local_path):
    print('Downloading file from S3')
    get_s3_client().download_file('lrtm.bot', s3_path, str(local_path))


def read_ads_csv(csv_path):
    csv_path = Path(csv_path)
    if is_s3_available():
        # TODO: The command will probably fail if the file does not already exist in S3
        if csv_path.exists():
            csv_path.unlink()
        download_from_s3(csv_path.name, csv_path)
    if not os.path.exists(csv_path):
        return pd.DataFrame()
    return pd.read_csv(csv_path, index_col=0)


def write_ads_csv(df, csv_path):
    csv_path = Path(csv_path)
    df.to_csv(csv_path)
    if is_s3_available():
        upload_to_s3(csv_path, csv_path.name)

from contextlib import contextmanager
import os
from pathlib import Path
import subprocess
import signal
import time

import requests

from lbc.utils import REPO_DIR, print


def get_public_ip_address():
    try:
        response = requests.get('https://ifconfig.me')
        response.raise_for_status()
        return response.text
    except Exception as e:  # Often timeout error
        print(e)
        return get_public_ip_address()


def get_openvpn_path():
    return Path(subprocess.Popen('which openvpn', shell=True, stdout=subprocess.PIPE).stdout.read().decode().strip())


def is_root():
    return os.geteuid() == 0


@contextmanager
def openvpn(config_path, authentication_path):
    assert is_root(), 'This methods needs to be run with sudo privileges and with the user environment: use `sudo -E`'
    config_path = Path(config_path)
    assert config_path.is_file()
    assert authentication_path.is_file()
    assert get_openvpn_path().is_file()
    command = f'cd {config_path.parent} && sudo {get_openvpn_path()} --config "{config_path.name}" --auth-user-pass {authentication_path}'
    initial_ip_address = get_public_ip_address()
    # https://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
    # The os.setsid() is passed in the argument preexec_fn so
    # it's run after the fork() and before  exec() to run the shell.
    print('Connecting to VPN')
    process = subprocess.Popen(command, shell=True, preexec_fn=os.setsid, stdout=subprocess.DEVNULL)  # Start VPN
    try:
        while get_public_ip_address() == initial_ip_address:  # Wait for the VPN to be active
            time.sleep(1)
        print('Connected to VPN')
        yield
    finally:
        # Terminate VPN process group
        print('Quitting VPN')
        os.killpg(os.getpgid(process.pid), signal.SIGTERM)


if __name__ == '__main__':
    # Example usage
    print(get_public_ip_address())
    with openvpn(REPO_DIR / 'config/AT - Vienna @tigervpn.com.ovpn', REPO_DIR / 'config/auth.txt'):
        print(get_public_ip_address())
    print(get_public_ip_address())

import os
from functools import lru_cache

import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer

from lbc.scrapping import get_new_ads, update_with_new_ads
from lbc.utils import DATA_DIR, format_ad, trigger_alert, read_ads_csv, write_ads_csv, print


@lru_cache()
def get_gpu_benchmarks():
    url = "http://www.videocardbenchmark.net/gpu_list.php"
    df_gpu = pd.read_html(url, attrs={"id": "cputable"})[0]
    # Clean column names
    new_cols = []
    for col in df_gpu.columns:
        col = col.split("\n")[0].lower().replace(" ", "_")
        new_cols.append(col)
    df_gpu.columns = new_cols
    return df_gpu


def clean_ad_title(ad_title, vocabulary=None):
    ad_title = ad_title.lower()
    ad_title = ad_title.replace("0ti", "0 ti")
    words = ad_title.split()
    not_meaningful_words = ['carte', 'graphique']
    words = [w for w in words if w not in not_meaningful_words]
    words = [w for w in words if vocabulary is None or w in vocabulary]
    return " ".join(words)


def guess_gpu_model(query_text, gpu_names):
    vectorizer = TfidfVectorizer()
    gpu_embeddings = vectorizer.fit_transform([clean_ad_title(gpu_name) for gpu_name in gpu_names])
    vocabulary = set(vectorizer.get_feature_names())
    query_text = clean_ad_title(query_text, vocabulary)
    query_embedding = vectorizer.transform([query_text])
    # Get most relevant models with cosine similarity based on tfidf
    similarities = cosine_similarity(gpu_embeddings, query_embedding)
    [index] = np.argmax(similarities, axis=0)
    return gpu_names[index], similarities[index][0]


def guess_gpu_name_and_score(ad_title):
    df_gpu = get_gpu_benchmarks()
    guessed_gpu_name, similarity = guess_gpu_model(ad_title, df_gpu["videocard_name"])
    [passmark_score] = df_gpu[df_gpu["videocard_name"] == guessed_gpu_name]["passmark_g3d_mark_(higher_is_better)"].tolist()
    return guessed_gpu_name, similarity, passmark_score


def format_gpu_ad(row):
    text = format_ad(row)
    text += "Model: {}\n".format(row["videocard_name"])
    text += "Benchmark: {0:.0f} - Ratio: {1:.0f}\n".format(
        row["passmark_g3d_mark_(higher_is_better)"], row["perf_price_ratio"])
    return text


def get_new_gpu_ads():
    query = '("carte graphique" OR NVIDIA OR GTX) (NOT Portable NOT PC NOT i5 NOT i7 NOT SSD NOT Processeur NOT 1989)'
    csv_path = os.path.join(DATA_DIR, "gpu.csv")
    df = read_ads_csv(csv_path)
    df_new_ads = get_new_ads(query)
    new_ids = set(df_new_ads.index) - set(df.index)
    print(f'Collected {len(new_ids)} new GPU ads')
    df = update_with_new_ads(df, df_new_ads)
    if len(new_ids) == 0:
        return
    # Guess the true GPU model based on the title
    df.loc[new_ids, "videocard_name"], df.loc[new_ids, "similarity"], df.loc[new_ids, "passmark_g3d_mark_(higher_is_better)"] = zip(
            *df.loc[new_ids, "title"].map(guess_gpu_name_and_score)
    )
    df.loc[new_ids, "perf_price_ratio"] = df.loc[new_ids, "passmark_g3d_mark_(higher_is_better)"] / df.loc[new_ids, "price"]
    # Send alert for interesting new ads
    alert_func = lambda row: ((row["perf_price_ratio"] >= 80) and  # noqa: E731
                              (row["videocard_name"] != "NVIDIA TITAN X") and
                              (row["passmark_g3d_mark_(higher_is_better)"] > 10000) and
                              (row["price"] > 30))
    trigger_alert(df.loc[new_ids], alert_func, alert_name='GPU',
                  format_ad_method=format_gpu_ad)
    write_ads_csv(df, csv_path)
